package es.correos.kata.gestortextos.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(AuditoriaComunicacionId.class)
@Table(name = "TpTmAuditoriaComunicacion")
public class TpTmAuditoriaComunicacion {

	@Id
	@Column(nullable = false)
	private Long codAuditoria;

	@Column(nullable = false)
	private String codProducto;

	@Id
	@Temporal(TemporalType.TIMESTAMP)
	@Column(nullable = false)
	private Date fecAlta;

	@Column(nullable = false)
	private String codRazon;

	@Column(nullable = false)
	private String desTag;

	@Column(nullable = false)
	private String codEnvio;

	@Column(nullable = false)
	private String codCliente;

	@Column(nullable = false)
	private String codSituacion;
}
