package es.correos.kata.gestortextos.domain;

import java.io.Serializable;
import java.util.Date;

public class AuditoriaComunicacionId implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private long codAuditoria;
	private Date fecAlta;

	public AuditoriaComunicacionId() {

	}
}
