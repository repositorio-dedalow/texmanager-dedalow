package es.correos.kata.gestortextos.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@IdClass(AvisoComunicacionId.class)
@Table(name = "TpTmAvisoComunicacion")
public class TpTmAvisoComunicacion {

	@Id
	@Column(nullable = false)
	private Long codAvisoComunicacion;

	@Column(nullable = false)
	private String textoGenreadoSMS;

	@Id
	@Column(name = "cod_auditoria", nullable = false)
	private long codAuditoria;

	@Column(nullable = false)
	private String textoGeneradoEmail;

	@Column(nullable = false)
	private String tipoAviso;

	@Column(nullable = false)
	private String desAsuntoEmail;

	@Column(nullable = false)
	private String destinaSMS;

	@Column(nullable = false)
	private String codIdioma;

	@Column(nullable = false)
	private String tipoComunicacion;

	@Column(nullable = false)
	private Integer plazoComunicacion;

	@Column(nullable = false)
	private String destinaEmail;

	@Id
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "fec_alta", nullable = false)
	private Date fecAlta;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "cod_auditoria", updatable = false, insertable = false)
	@JoinColumn(name = "fec_alta", updatable = false, insertable = false)
	private TpTmAuditoriaComunicacion auditoria;
}
