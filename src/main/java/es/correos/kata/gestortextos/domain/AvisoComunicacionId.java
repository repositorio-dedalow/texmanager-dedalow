package es.correos.kata.gestortextos.domain;

import java.io.Serializable;
import java.util.Date;

public class AvisoComunicacionId implements Serializable {

	private static final long serialVersionUID = -1580317182342745620L;

	private long codAuditoria;
	private Date fecAlta;
	private long codAvisoComunicacion;
	
	public AvisoComunicacionId() {
		
	}
}
