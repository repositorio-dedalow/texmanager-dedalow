package es.correos.kata.gestortextos.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "TbTmTextoComunicacion")
public class TbTmTextoComunicacion {

  @Column(nullable = false)
  private Date fecAlta;

  @Column(nullable = false)
  private String desTextoSMS;

  @Id
  @Column(nullable = false)
  private Integer codTextoComunicacion;

  @Column(nullable = false)
  private String codCliente;

  @Column(nullable = false)
  private String codAplicacion;

  @Column(nullable = false)
  private String indCliente;

  @Column(nullable = false)
  private String codIdioma;

  @Column(nullable = false)
  private String plazoComunicacion;

  @Column(nullable = false)
  private String codProducto;

  @Column(nullable = false)
  private String codSituacion;

  @Column(nullable = false)
  private String desAsuntoEmail;

  @Column(nullable = false)
  private String codTextoExt;

  @Column(nullable = false)
  private String codRazon;

  @Column(nullable = false)
  private String desTextoEmail;
}
