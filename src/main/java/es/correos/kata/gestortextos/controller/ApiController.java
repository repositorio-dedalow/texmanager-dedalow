package es.correos.kata.gestortextos.controller;

import es.correos.arch.boot.core.jwt.service.JWTTokenService;
import es.correos.kata.gestortextos.dto.MensajeRODADTO;
import es.correos.kata.gestortextos.dto.ResultDTO;
import es.correos.kata.gestortextos.service.ApiService;
import io.swagger.annotations.Api;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Log4j2
@RequiredArgsConstructor
@RestController
@RequestMapping("/api")
@Api
public class ApiController {

  private final ApiService apiService;

  private final JWTTokenService tokenService;

  @PostMapping(path = "/communication-reciever")
  public ResponseEntity<ResultDTO> communicationReciever(MensajeRODADTO mensajeRoda) {
    return ResponseEntity.ok(apiService.communicationRecieverService(mensajeRoda));
  }

  @PostMapping(path = "/calendarizador")
  public ResponseEntity<ResultDTO> calendarizador() {
    return ResponseEntity.ok(apiService.calendarizadorService());
  }

  @PostMapping(path = "/search-audit")
  public ResponseEntity<MensajeRODADTO> searchAudit(Long codAuditoria) {
    return ResponseEntity.ok(apiService.searchAuditService(codAuditoria));
  }
}
