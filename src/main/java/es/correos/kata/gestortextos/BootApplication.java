package es.correos.kata.gestortextos;

import es.correos.arch.boot.core.annotations.CorreosBootConfiguration;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableConfigurationProperties
@CorreosBootConfiguration
@EnableScheduling
// No es necesario. Si está incluído el módulo feign-core, automáticamente
// se habilitan los clientes feign a partir del package es.correos.
// @EnableFeignClients
@SpringBootApplication
public class BootApplication implements CommandLineRunner {

  public static void main(String[] args) {
    SpringApplication.run(BootApplication.class, args);
  }

  @Override
  public void run(String... args) throws Exception {
    // TODO Auto-generated method stub

  }
}
