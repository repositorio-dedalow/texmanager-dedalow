package es.correos.kata.gestortextos.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TpTmAuditoriaComunicacionDTO implements Serializable {

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Integer codAuditoria;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Integer codProducto;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Date fecAlta;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String codRazon;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String desTag;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String codCliente;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String codSituacion;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String codEnvio;

  private List<TpTmAvisoComunicacionDTO> tpTmAvisoComunicacion = new ArrayList<>();
}
