package es.correos.kata.gestortextos.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ComunicacionDTO implements Serializable {

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String tipoAviso;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String idioma;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String email;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String telefono;
}
