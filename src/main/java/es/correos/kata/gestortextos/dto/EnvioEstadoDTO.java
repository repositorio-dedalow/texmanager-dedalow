package es.correos.kata.gestortextos.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EnvioEstadoDTO implements Serializable {

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Date date;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String code;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String reason_code;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String origin_postal_office;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String destination_postal_office;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String expiration_date;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String reporter_node;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String situation_summary;
}
