package es.correos.kata.gestortextos.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MensajeGNOMODTO implements Serializable {

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String aplicacionOrigen;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String aplicacionDestino;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String mensajeUnico;
}
