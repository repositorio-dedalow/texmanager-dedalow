package es.correos.kata.gestortextos.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class MensajeRODADTO implements Serializable {

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String codShipping;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String codSituation;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String codResult;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String language;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Date date;

  private List<TagDTO> tag = new ArrayList<>();
  private List<ComunicacionDTO> comunicacion = new ArrayList<>();
}
