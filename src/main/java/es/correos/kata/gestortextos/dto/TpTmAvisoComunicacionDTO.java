package es.correos.kata.gestortextos.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TpTmAvisoComunicacionDTO implements Serializable {

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Long codAvisoComunicacion;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String textoGeneradoSms;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String codAuditoria;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String textoGeneradoEmail;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String tipoAviso;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String desAsuntoEmail;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String destinaSms;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String codIdioma;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Integer plazoComunicacion;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Integer tipoComunicacion;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String destinaEmail;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Date fecAlta;
}
