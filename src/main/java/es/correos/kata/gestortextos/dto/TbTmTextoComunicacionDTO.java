package es.correos.kata.gestortextos.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import java.io.Serializable;
import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TbTmTextoComunicacionDTO implements Serializable {

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Integer codTextoComunicacion;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String codAplicacion;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String codIdioma;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String codProducto;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String codSituacion;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String codTextoExt;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private Date fecAlta;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String desTextoSMS;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String codCliente;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String indCliente;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String plazoComunicacion;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String desAsuntoEmail;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String codRazon;

  @JsonInclude(JsonInclude.Include.NON_NULL)
  private String desTextoEmail;
}
